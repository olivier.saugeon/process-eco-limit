/*
 * process-eco-limit
 * Copyright (C) 2019-2020  Olivier SAUGEON
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.saugeon.olivier.process.ecolimit.scheduler;

import java.util.HashSet;
import java.util.Set;
import java.util.TimerTask;

import fr.saugeon.olivier.process.ecolimit.process.IProcess;

public class Scheduler extends TimerTask {

	private Set<IConstraint> constraints = new HashSet<IConstraint>();

	private IProcess process;

	public void addContraint(IConstraint constraint) {
		this.constraints.add(constraint);
	}

	public void setProcess(IProcess process) {
		this.process = process;
	}

	public boolean isRunnable() {
		return this.process != null;
	}

	@Override
	public void run() {
		if (this.canRun()) {
			this.process.start();
		} else {
			this.process.stop();
		}
	}

	private boolean canRun() {
		for (IConstraint constraint : this.constraints) {
			if (!constraint.isValid()) {
				return false;
			}
		}
		return true;
	}

}
