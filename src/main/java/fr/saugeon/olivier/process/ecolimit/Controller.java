/*
 * process-eco-limit
 * Copyright (C) 2019-2020  Olivier SAUGEON
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.saugeon.olivier.process.ecolimit;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Timer;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import fr.saugeon.olivier.process.ecolimit.process.RunProcess;
import fr.saugeon.olivier.process.ecolimit.process.StartStopProcess;
import fr.saugeon.olivier.process.ecolimit.scheduler.MaxPercentageConstraint;
import fr.saugeon.olivier.process.ecolimit.scheduler.OnAcPowerConstraint;
import fr.saugeon.olivier.process.ecolimit.scheduler.Scheduler;

public class Controller {

	// help option.
	public static final String HELP_OPTION = "help";

	// scheduler options...
	public static final String SCHEDULER_REFRESH_PERIOD_OPTION = "refresh-period";
	public static final String SCHEDULER_CONFIGURATION_FILE_OPTION = "configuration-file";

	// constraint options...
	public static final String CONSTRAINT_MAX_PERCENTAGE_OPTION = "max-percentage";
	public static final String CONSTRAINT_ON_AC_POWER_OPTION = "on-ac-power";

	// process control options...
	public static final String PROCESS_RUN_COMMAND_OPTION = "run";
	public static final String PROCESS_START_COMMAND_OPTION = "start";
	public static final String PROCESS_STOP_COMMAND_OPTION = "stop";

	// command line options
	private Options options = new Options();

	// command line parser
	private CommandLineParser parser = new DefaultParser();

	// scheduler refresh period (in seconds)
	// by default refresh every minute
	private int refresh = 60;

	// process scheduler
	private Scheduler scheduler = new Scheduler();

	public static Controller build() {
		return new Controller();
	}

	public Controller() {
		options.addOption(Option.builder().longOpt(Controller.HELP_OPTION).desc("print help message").build());

		options.addOption(Option.builder().longOpt(Controller.SCHEDULER_REFRESH_PERIOD_OPTION).hasArg().argName("SEC")
				.desc("scheduler refresh period in sec").build());
		options.addOption(Option.builder().longOpt(Controller.SCHEDULER_CONFIGURATION_FILE_OPTION).hasArg()
				.argName("PATH").desc("scheduler configuration file").build());

		options.addOption(Option.builder().longOpt(Controller.CONSTRAINT_MAX_PERCENTAGE_OPTION).hasArg().argName("PCT")
				.desc("run process under PCT % of time").build());
		options.addOption(Option.builder().longOpt(Controller.CONSTRAINT_ON_AC_POWER_OPTION)
				.desc("run process on AC power only").build());

		options.addOption(Option.builder().longOpt(Controller.PROCESS_RUN_COMMAND_OPTION).hasArg().argName("CMD")
				.desc("command to run process").build());
		options.addOption(Option.builder().longOpt(Controller.PROCESS_START_COMMAND_OPTION).hasArg().argName("CMD")
				.desc("command to start process").build());
		options.addOption(Option.builder().longOpt(Controller.PROCESS_STOP_COMMAND_OPTION).hasArg().argName("CMD")
				.desc("command to stop process").build());
	}

	public Options getOptions() {
		return this.options;
	}

	public Controller withConf(Path conf) {
		System.out.println(conf.toString());
		try {
			return this.withArgs(Files.readAllLines(conf).toArray(new String[] {}));
		} catch (IOException e) {
			System.err.println("Failed to read configuration file " + conf);
			e.printStackTrace();
		}
		return this;
	}

	public Controller withArgs(String[] args) {
		for (String arg : args) {
			System.out.println(arg);
		}
		try {
			// parse the command line arguments
			return this.withCommandLine(parser.parse(options, args));
		} catch (ParseException e) {
			System.err.println("Failed to parse command line arguments");
			e.printStackTrace();
			return this.help();
		}
	}

	public Controller withCommandLine(CommandLine line) {

		// get scheduler refresh period
		if (line.hasOption(SCHEDULER_REFRESH_PERIOD_OPTION)) {
			try {
				this.refresh = Integer.parseInt(line.getOptionValue(SCHEDULER_REFRESH_PERIOD_OPTION));
			} catch (NumberFormatException e) {
				System.err.println("Failed to parse " + SCHEDULER_REFRESH_PERIOD_OPTION + " option value");
				e.printStackTrace();
				return this.help();
			}
		}
		if (line.hasOption(SCHEDULER_CONFIGURATION_FILE_OPTION)) {
			try {
				return this.withArgs( //
						Files.readAllLines( //
								Paths.get( //
										line.getOptionValue(SCHEDULER_CONFIGURATION_FILE_OPTION) //
								) //
						) //
								.toArray(new String[] {}) //
				);
			} catch (IOException e) {
				System.err.println("Failed to read " + SCHEDULER_CONFIGURATION_FILE_OPTION + " option value");
				e.printStackTrace();
				return this.help();
			}
		}

		// add constraints
		if (line.hasOption(CONSTRAINT_MAX_PERCENTAGE_OPTION)) {
			scheduler.addContraint(new MaxPercentageConstraint( //
					line.getOptionValue(CONSTRAINT_MAX_PERCENTAGE_OPTION) //
			));
		}
		if (line.hasOption(CONSTRAINT_ON_AC_POWER_OPTION)) {
			scheduler.addContraint(new OnAcPowerConstraint());
		}

		// create process
		if (line.hasOption(PROCESS_RUN_COMMAND_OPTION)) {
			scheduler.setProcess(new RunProcess( //
					line.getOptionValue(PROCESS_RUN_COMMAND_OPTION) //
			));
		} else if ( //
		line.hasOption(PROCESS_START_COMMAND_OPTION) //
				&& line.hasOption(PROCESS_STOP_COMMAND_OPTION) //
		) {
			scheduler.setProcess(new StartStopProcess( //
					line.getOptionValue(PROCESS_START_COMMAND_OPTION), //
					line.getOptionValue(PROCESS_STOP_COMMAND_OPTION) //
			));
		} else {
			return this.help();
		}

		return this;
	}

	public Controller help() {
		// automatically generate the help statement
		HelpFormatter formatter = new HelpFormatter();
		// print the help message
		formatter.printHelp("java -jar process-eco-limit-{version}-jar-with-dependencies.jar", options);
		return this;
	}

	public Controller run() {
		if (this.scheduler.isRunnable()) {
			Timer timer = new Timer();
			timer.scheduleAtFixedRate(this.scheduler, 0, this.refresh * 1000);
		}
		return this;
	}

}
