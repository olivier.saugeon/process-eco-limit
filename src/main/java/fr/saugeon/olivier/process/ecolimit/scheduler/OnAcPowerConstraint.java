/*
 * process-eco-limit
 * Copyright (C) 2019-2020  Olivier SAUGEON
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.saugeon.olivier.process.ecolimit.scheduler;

import java.io.IOException;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;

public class OnAcPowerConstraint implements IConstraint {

	private DefaultExecutor executor = new DefaultExecutor();

	@Override
	public boolean isValid() {
		try {
			executor.execute(CommandLine.parse("on_ac_power"));
			return true;
		} catch (IOException e) {
		}
		return false;
	}

}
