/*
 * process-eco-limit
 * Copyright (C) 2019-2020  Olivier SAUGEON
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.saugeon.olivier.process.ecolimit.process;

import java.io.IOException;
import java.time.Instant;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;

public class RunProcess implements IProcess {

	private CommandLine command;
	private DefaultExecuteResultHandler handler = new DefaultExecuteResultHandler();
	private DefaultExecutor executor = new DefaultExecutor();
	private boolean running = false;

	public static RunProcess build(String runCommand) {
		return new RunProcess(runCommand);
	}

	public RunProcess(String runCommand) {
		String[] args = runCommand.split(" ");
		this.command = new CommandLine(args[0]);
		for (int i = 1; i < args.length; i++) {
			command.addArgument(args[i]);
		}
		ExecuteWatchdog watchdog = new ExecuteWatchdog(ExecuteWatchdog.INFINITE_TIMEOUT);
		executor.setWatchdog(watchdog);
	}

	public void start() {
		if (!this.running) {
			System.out.println("-------------------------------------------");
			System.out.println(Instant.now().toString() + " starting...");
			System.out.println("-------------------------------------------");
			try {
				executor.execute(this.command, handler);
				this.running = true;
			} catch (IOException e) {
				System.out.println("Failed to start process");
				e.printStackTrace();
			}
		}
	}

	public void stop() {
		if (this.running) {
			System.out.println("-------------------------------------------");
			System.out.println(Instant.now().toString() + " stoping...");
			System.out.println("-------------------------------------------");
			executor.getWatchdog().destroyProcess();
			this.running = false;
		}
	}

}
