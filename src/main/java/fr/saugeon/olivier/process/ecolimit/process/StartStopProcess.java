/*
 * process-eco-limit
 * Copyright (C) 2019-2020  Olivier SAUGEON
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.saugeon.olivier.process.ecolimit.process;

import java.io.IOException;
import java.time.Instant;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;

public class StartStopProcess implements IProcess {

	private String startCommand;
	private String stopCommand;
	private DefaultExecutor executor = new DefaultExecutor();

	public static StartStopProcess build(String startCommand, String stopCommand) {
		return new StartStopProcess(startCommand, stopCommand);
	}

	public StartStopProcess(String startCommand, String stopCommand) {
		this.startCommand = startCommand;
		this.stopCommand = stopCommand;
	}

	public void start() {
		System.out.println("-------------------------------------------");
		System.out.println(Instant.now().toString() + " starting...");
		System.out.println("-------------------------------------------");
		try {
			executor.execute(CommandLine.parse(startCommand));
		} catch (IOException e) {
			System.out.println("Failed to start process");
			e.printStackTrace();
		}
	}

	public void stop() {
		System.out.println("-------------------------------------------");
		System.out.println(Instant.now().toString() + " stoping...");
		System.out.println("-------------------------------------------");
		try {
			executor.execute(CommandLine.parse(stopCommand));
		} catch (IOException e) {
			System.out.println("Failed to start process");
			e.printStackTrace();
		}
	}

}
