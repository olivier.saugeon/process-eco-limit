/*
 * process-eco-limit
 * Copyright (C) 2019-2020  Olivier SAUGEON
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.saugeon.olivier.process.ecolimit.scheduler;

import java.util.concurrent.ArrayBlockingQueue;

public class MaxPercentageConstraint implements IConstraint {

	private ArrayBlockingQueue<Boolean> history = new ArrayBlockingQueue<Boolean>(100);

	private int max;

	public MaxPercentageConstraint(String max) {
		this.max = Integer.parseInt(max);
	}

	private int computeHistoryPercentage() {
		int sum = 0;
		for (Boolean b : history) {
			if (b) {
				sum++;
			}
		}
		int precentage = 0;
		if (history.size() > 0) {
			precentage = sum * 100 / (history.size());
		}
		return precentage;
	}

	@Override
	public boolean isValid() {
//		System.out.println(Instant.now().toString() + " " + history.toString());
		boolean run = false;
		if (this.computeHistoryPercentage() < this.max) {
			run = true;
		}
		if (history.remainingCapacity() == 0) {
			history.poll();
		}
		history.offer(run);
		return run;
	}

}
