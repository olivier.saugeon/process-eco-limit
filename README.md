
Process Eco Limit
=================

Java program to control a child process based on environmental and economical criteria.

Usage
-----

`java -jar process-eco-limit-{version}-jar-with-dependencies.jar [OPTIONS]`

###### Options

* `--help`
print help message

* `--refresh-period <SEC>`
scheduler refresh period in sec
* `--configuration-file <PATH>`
scheduler configuration file

* `--max-percentage <PCT>`
run process under PCT % of time (computed on the last 100 periods)

* `--on-ac-power`
run process on AC power only (using `/usr/bin/on_ac_power`)
* `--run <CMD>`
command to run process
* `--start <CMD>`
command to start process
* `--stop <CMD>`
command to stop process

Dependencies
------------

* Apache Commons CLI https://commons.apache.org/proper/commons-cli/
* Apache Commons Exec https://commons.apache.org/proper/commons-exec/

License
-------

GNU GPLv3. See LICENSE file.
